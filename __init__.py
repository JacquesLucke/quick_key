# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Quick Key",
    "author" : "Jacques Lucke",
    "description" : "",
    "blender" : (4, 0, 0),
    "version" : (0, 0, 1),
    "location" : "",
    "warning" : "",
    "category" : "Workflow"
}

import bpy
from bpy.props import BoolProperty, StringProperty, PointerProperty, CollectionProperty

class QuickKeyEntry(bpy.types.PropertyGroup):
    def update(self, context):
        update_keymaps()

    key_combination: StringProperty(
        name="Key Combination",
        update=update,
    )

    node_idname: StringProperty(
        name="Node",
        update=update,
    )

class QuickKeyPreferences(bpy.types.AddonPreferences):
    bl_idname = "quick_key"

    entries: CollectionProperty(type=QuickKeyEntry)

    some_option: BoolProperty(name="Some Option")

    def draw(self, context):
        layout = self.layout

        col = layout.column()
        for entry in self.entries:
            subcol = col.column(align=True)
            subcol.prop(entry, "key_combination", text="Keys")
            subcol.prop(entry, "node_idname", text="Node")

        layout.operator("quick_key.add")

class AddEntryOperator(bpy.types.Operator):
    bl_idname = "quick_key.add"
    bl_label = "Add Quick Key"

    def execute(self, context):
        prefs = context.preferences.addons["quick_key"].preferences
        prefs.entries.add()
        update_keymaps()
        return {'FINISHED'}


def build_menu(key_combination):
    def draw(self, context):
        layout = self.layout
        layout.operator_context = 'INVOKE_DEFAULT'
        prefs = context.preferences.addons["quick_key"].preferences
        submenus = set()
        for entry in prefs.entries:
            entry_key_combination = entry.key_combination.upper()
            if not entry_key_combination.startswith(key_combination):
                continue
            if len(key_combination) + 1 == len(entry_key_combination):
                props = layout.operator(
                    "node.add_node",
                    text=entry_key_combination[-1]
                )
                props.use_transform = True
                props.type = entry.node_idname
            elif len(key_combination) + 1 < len(entry_key_combination):
                submenus.add(entry_key_combination[:len(key_combination)+1])
        for submenu in submenus:
            layout.menu(
                "QUICK_KEY_MT_menu_" + submenu,
                text=submenu[len(key_combination)]
            )


    idname = "QUICK_KEY_MT_menu_" + key_combination
    return type(
        idname,
        (bpy.types.Menu, ),
        {
            "bl_idname": idname,
            "bl_label": "Quick Key Menu",
            "draw": draw,
        }
    )


menus = []
keymaps = []

def unregister_keymaps():
    wm = bpy.context.window_manager
    for km in keymaps:
        wm.keyconfigs.addon.keymaps.remove(km)
    keymaps.clear()
    for menu in menus:
        bpy.utils.unregister_class(menu)
    menus.clear()

def register_keymaps():
    wm = bpy.context.window_manager
    prefs = bpy.context.preferences.addons["quick_key"].preferences
    start_keys = set()
    menu_keys = set()
    for entry in prefs.entries:
        if len(entry.key_combination) >= 2:
            key_combination = entry.key_combination.upper()
            start_keys.add(key_combination[0])
            for i in range(1, len(key_combination)):
                menu_keys.add(key_combination[:i])
    for menu_key in menu_keys:
        menu = build_menu(menu_key)
        bpy.utils.register_class(menu)
        menus.append(menu)
    for start_key in start_keys:
        km = wm.keyconfigs.addon.keymaps.new(name="Node Editor", space_type="NODE_EDITOR")
        keymaps.append(km)
        kmi = km.keymap_items.new("wm.call_menu", start_key, 'PRESS', ctrl=False, shift=False)
        kmi.properties.name = "QUICK_KEY_MT_menu_" + start_key

def update_keymaps():
    unregister_keymaps()
    register_keymaps()


classes = [
    QuickKeyEntry,
    QuickKeyPreferences,
    AddEntryOperator,
]

def register():
    print("REGISTER")
    for cls in classes:
        bpy.utils.register_class(cls)
    register_keymaps()

def unregister():
    print("UN-REGISTER")
    unregister_keymaps()
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
