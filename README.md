# Quick Key

This is a tech demo for functionality that allows setting up multi-key shortcuts. The goal is to provide a better alternative to accelerator keys in Blender.

Advantages:
* Setting up the key combinations manually, makes it impossible that changes in menus breaks the shortcuts which happens with accelerator keys.
* One can choose to use only a few keys that are close to each to map to many different operators. This can be much faster once learned, because accelerator keys are essentially randomly scattered on the keyboard.

The only disadvantage is that these key combinations have to be setup manually and don't work automatically everywhere. That's not a big regression, because the keys have to be learned one by one anyway.

-----

This is only a tech demo. It would be nice if someone could develop this further into a standalone addon, or if we can work out a design so that we can integrate this into Blender natively.
